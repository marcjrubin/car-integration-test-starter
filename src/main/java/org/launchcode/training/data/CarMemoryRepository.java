package org.launchcode.training.data;


import org.launchcode.training.models.Car;

import java.util.ArrayList;
import java.util.List;

public class CarMemoryRepository {

    private static ArrayList<Car> cartStorage = new ArrayList<>();

    public List<Car> findAll() {
        return new ArrayList<>(cartStorage);
    }

    public void save(Car cart) {
        cartStorage.add(cart);
    }

    public void clear() {
        cartStorage.clear();
    }

}
